defmodule RemoteClass do
  use GenServer

  def execute(pid, function, args) do
    GenServer.call(pid, {:execute, function, args}, 5000)
  end

  def init([state, module]) do
    IO.inspect state
    state =module.init(state)
    {:ok, %{state: state, module: module }}
  end

  @impl true
  def handle_call({:execute, function, args}, _from, %{state: state, module: module }) do
    {state, return} = apply(module, String.to_atom(function), [state | args])
    {:reply, return, %{state: state, module: module }}
  end
end